#!/bin/bash

find . -name "*.pyc" -exec rm {} +
find . -name "bgc_seeds.hmm" -exec rm {} +
find . -name "*.h3?" -exec rm {} +
find . -name "*.tar.*" -exec rm {} +
for d in $(find . -name "index.html"); do
    DIR=$(dirname $d)
    rm -r $DIR
done

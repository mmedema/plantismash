# vim: set fileencoding=utf-8 :
#
# Copyright (C) 2016 Marnix H. Medema
# Wageningen University, Bioinformatics Group
#
# Copyright (C) 2016 Marc Chevrette
# University of Wisconsin, Madison; Currie Lab
#
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.
'''
Terpene cyclization pattern prediction based on terpene synthase amino acid
sequences.
'''

import logging
from Bio.SeqFeature import SeqFeature, FeatureLocation
from antismash import utils
from helperlibs.wrappers.io import TemporaryDirectory
import os
import sys


def specific_analysis(seq_record, options):
    utils.log_status("Calulating detailed predictions for Terpene clusters.")
    #Find terpene clusters
    terpene_clusters = utils.get_cluster_features_of_type(seq_record, "terpene")
    #For each, find canonical terpene synthases
    terpene_synthases = find_terpene_synthases(terpene_clusters, seq_record)
    #For each TS, run predictor
    outputs = run_terp_predicat(terpene_synthases)
    #Parse and store output in seq_record object
    store_terp_predicat_results(terpene_synthases, outputs)


def find_terpene_synthases(clusters, seq_record):
    """Find terpene synthase-encoding CDS features"""
    terpene_synthases = []
    for cluster in clusters:
        for feature in utils.get_cluster_cds_features(cluster, seq_record):
            if 'sec_met' in feature.qualifiers:
                for annotation in feature.qualifiers['sec_met']:
                    if "Domains detected:" in annotation and "Terpene" in annotation:
                        terpene_synthases.append(feature)
                        break
    return terpene_synthases

def run_terp_predicat(terpene_synthases):
    """Run Terpene PrediCAT for each terpene synthase"""
    outputs = {}
    terp_predicat_dir = os.path.dirname(os.path.abspath(__file__))
    with TemporaryDirectory(change=True):
        for terpene_synthase in terpene_synthases:
            #Write CDS feature to FASTA file
            terp_seqs_file = "input_tps.fasta"
            terp_seqs = [str(utils.get_aa_sequence(terpene_synthase))]
            terp_names = [utils.get_gene_id(terpene_synthase)]
            utils.writefasta(terp_names, terp_seqs, terp_seqs_file)
            #Run PrediCAT
            predicat_command = [terp_predicat_dir + os.sep + 'predictterp_nodep.sh', 'input_tps.fasta', terp_predicat_dir]
            out, err, retcode = utils.execute(predicat_command)
            if retcode != 0:
                logging.error("Failed to run PrediCAT for terpenes: %r" % err)
                sys.exit(1)
            outputs[utils.get_gene_id(terpene_synthase)] = out.partition("\n")[2]
            os.remove(terp_seqs_file)
    return outputs


def store_terp_predicat_results(terpene_synthases, outputs):
    """Parses output and stores it in the terpene CDS features"""
    terp_annotationdict = {}
    for line in open(utils.get_full_path(__file__, "terpene_synthase_annotations_vs_accessions.txt"), "r"):
        line = line.rstrip('\n')
        val, key = line.split('\t')[:2]
        terp_annotationdict[key] = val

    for terpene_synthase in terpene_synthases:
        if 'note' not in terpene_synthase.qualifiers:
            terpene_synthase.qualifiers['note'] = []
        output = outputs[utils.get_gene_id(terpene_synthase)]
        parts = output.replace("\n", "").split("\t")
        besthit_acc = parts[11].partition("_")[0].replace("-", "_")
        besthit_name = terp_annotationdict.get(besthit_acc, "No BLAST hit")
        if parts[1] == "no_confident_result":
            if 'note' not in terpene_synthase.qualifiers:
                terpene_synthase.qualifiers['note'] = []
            terpene_synthase.qualifiers['note'].append("Cyclization pattern: no prediction")
            terpene_synthase.qualifiers['note'].append("Nearest neighbour: none found")
            if besthit_name == "No BLAST hit":
                terpene_synthase.qualifiers['note'].append("Best known BLAST hit: %s" % (besthit_name))
            else:
                terpene_synthase.qualifiers['note'].append("Best known BLAST hit: %s (%s, %s %%ID)" % (besthit_name, besthit_acc, parts[10]))
        else:
            cycl_pattern, nn, pid = parts[1], parts[9] + ", " + parts[7], str(parts[10])
            if cycl_pattern == 'no_confident_result':
                cycl_pattern = 'unknown'
                nn = 'none found'
            if 'note' not in terpene_synthase.qualifiers:
                terpene_synthase.qualifiers['note'] = []
            terpene_synthase.qualifiers['note'].append("Cyclization pattern: %s" % cycl_pattern)
            terpene_synthase.qualifiers['note'].append("Nearest neighbour: %s (%s)" % (terp_annotationdict[nn.partition(", ")[0]], nn))
            if besthit_name == "No BLAST hit":
                terpene_synthase.qualifiers['note'].append("Best known BLAST hit: %s" % (besthit_name))
            else:
                terpene_synthase.qualifiers['note'].append("Best known BLAST hit: %s (%s, %s %%ID)" % (besthit_name, besthit_acc, parts[10]))


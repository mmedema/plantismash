try:
    import unittest2 as unittest
except ImportError:
    import unittest
from Bio.SeqUtils.ProtParam import ProteinAnalysis
from Bio.SeqFeature import SeqFeature, FeatureLocation
from minimock import mock, restore, TraceTracker
from antismash import utils
from antismash.specific_modules.lassopeptides.specific_analysis import (
    Lassopeptide,
    predict_cleavage_site,
    result_vec_to_features,
)

class TestLassopeptide(unittest.TestCase):
    def test_init(self):
        "Test Lassopeptide instantiation"
        lasso = Lassopeptide(23, 42, 17, 51)
        self.assertTrue(isinstance(lasso, Lassopeptide))
        self.assertEqual(23, lasso.start)
        self.assertEqual(42, lasso.end)
        self.assertEqual(17, lasso.score)
        self.assertEqual('Class-II', lasso._lassotype)
        self.assertEqual('', lasso.core)
        self.assertEqual(51, lasso.rodeo_score)
        with self.assertRaises(ValueError):
            lasso.molecular_weight

    def test_repr(self):
        "Test Lassopeptide representation"
        lasso = Lassopeptide(23, 42, 17, 51)
        lasso.core = "ABCDEFGHIJKLM"
        expected = "Lassopeptide(23..42, 17, 'Class-II', 'ABCDEFGHIJKLM', 0, -1(-1), , )"
        self.assertEqual(expected, repr(lasso))

    def test_core(self):
        "Test Lassopeptide.core"
        lasso = Lassopeptide(23, 42, 17, 51)
        self.assertEqual('', lasso.core)
        self.assertFalse(hasattr(lasso, 'core_analysis'))
        lasso.core = "MAGICHAT"
        self.assertEqual('MAGICHAT', lasso.core)
        self.assertTrue(hasattr(lasso, 'core_analysis'))

    def test_core_ignore_invalid(self):
        "Test Lassopeptide.core ignores invalid amino acids"
        lasso = Lassopeptide(23, 42, 17, 51)
        self.assertEqual('', lasso.core)
        self.assertFalse(hasattr(lasso, 'core_analysis'))
        lasso.core = "MAGICXHAT"
        self.assertEqual('MAGICXHAT', lasso.core)
        self.assertTrue(hasattr(lasso, 'core_analysis'))

    def test_number_bridges_class(self):
        "Test Lassopeptide.number_bridges"
        lasso = Lassopeptide(23, 42, 20, 51)
        lasso.core = "MAGIXHAT"
        self.assertEqual(0, lasso.number_bridges)
        self.assertEqual('Class-II', lasso.lasso_class)
        lasso.core = "CAGIMHAC"
        self.assertEqual(1, lasso.number_bridges)
        self.assertEqual('Class-III', lasso.lasso_class)
        lasso.core = "CCCC"
        self.assertEqual(2, lasso.number_bridges)
        self.assertEqual('Class-I', lasso.lasso_class)

    def test_monoisotopic_mass(self):
        "Test Lassopeptide.monoisotopic_mass"
        lasso = Lassopeptide(23, 42, 17, 51)
        lasso.core = "MAGICHAT"
        analysis = ProteinAnalysis("MAGICHAT", monoisotopic=True)
        mw = analysis.molecular_weight()
        mw -= 18.02
        self.assertAlmostEqual(mw, lasso.monoisotopic_mass)
        self.assertAlmostEqual(mw, lasso._monoisotopic_weight)

        lasso._weight = 42
        self.assertEqual(42, lasso.molecular_weight)

    def test_molecular_weight(self):
        "Test Lassopeptide.molecular_weight"
        lasso = Lassopeptide(23, 42, 17, 51)
        lasso.core = "MAGICHAT"
        analysis = ProteinAnalysis("MAGICHAT", monoisotopic=False)
        mw = analysis.molecular_weight()
        mw -= 18.02
        self.assertAlmostEqual(mw, lasso.molecular_weight)
        self.assertAlmostEqual(mw, lasso._weight)

        lasso._weight = 42
        self.assertEqual(42, lasso.molecular_weight)

    def test_macrolactam(self):
        "Test Lassopeptide.macrolactam"
        lasso = Lassopeptide(23, 42, 20, 51)
        lasso.core = "GAAAAADLLLLLLLLL"
        self.assertEqual("GAAAAAD", lasso.macrolactam)
        
  
class TestSpecificAnalysis(unittest.TestCase):
    class FakeHit(object):
        class FakeHsp(object):
            def __init__(self, start, end, score):
                self.query_start = start
                self.query_end = end
                self.bitscore = score

        def __init__(self, start, end, score, desc):
            self.hsps = [ self.FakeHsp(start, end, score) ]
            self.description = desc

        def __iter__(self):
            return iter(self.hsps)

    def setUp(self):
        self.tt = TraceTracker()
        self.hmmpfam_return_vals = []
        mock('utils.run_hmmpfam2', tracker=self.tt, returns=self.hmmpfam_return_vals)

    def tearDown(self):
        restore()

    def test_predict_cleavage_site(self):
        "Test lassopeptides.predict_cleavage_site()"
        resvec = predict_cleavage_site('foo', 'bar', 51)
        self.assertEqual([None, None, None], resvec)
        fake_hit = self.FakeHit(24, 42, 17, 'Class-II')
        self.hmmpfam_return_vals.append([fake_hit, fake_hit])

        resvec = predict_cleavage_site('foo', 'bar', 51)
        self.assertEqual([None, None, None], resvec)

        start, end, score = predict_cleavage_site('foo', 'bar', 15)
        
        self.assertEqual(23, start)
        self.assertEqual(41, end)
        self.assertEqual(17, score)

    def test_result_vec_to_features(self):
        "Test lassopeptides.result_vec_to_features()"
        loc = FeatureLocation(0, 165, strand=1)
        orig_feature = SeqFeature(loc, 'CDS')
        orig_feature.qualifiers['locus_tag'] = ['FAKE0001']
        vec = Lassopeptide(17, 23, 42, 51)
        seq = "TAILTAILTAILTAILTAILTAILTAILTAILTAILCC"
        vec.core = seq
        vec.leader = "HEADHEADHEAD"
        new_features = result_vec_to_features(orig_feature, vec)
        self.assertEqual(2, len(new_features))
        leader, core = new_features

        self.assertEqual(loc.start, leader.location.start)
        self.assertEqual(loc.start + (23 * 3), leader.location.end)
        self.assertEqual(loc.strand, leader.location.strand)
        self.assertEqual('CDS_motif', leader.type)
        self.assertEqual(orig_feature.qualifiers['locus_tag'],
                         leader.qualifiers['locus_tag'])
        self.assertEqual(['leader peptide','lassopeptide',
                          'predicted leader seq: HEADHEADHEAD'], leader.qualifiers['note'])

        self.assertEqual(leader.location.end, core.location.start)
        self.assertEqual(loc.end, core.location.end)
        self.assertEqual(loc.strand, core.location.strand)
        self.assertEqual('CDS_motif', core.type)
        expected = ['core peptide','lassopeptide', 'monoisotopic mass: 3790.3',
                    'molecular weight: 3792.8', 'monoisotopic mass_cut: 3790.3',
                    'molecular weight_cut: 3792.8', 'number of bridges: 1',
                    'predicted core seq: TAILTAILTAILTAILTAILTAILTAILTAILTAILCC',
                    'predicted class: Class-III', 'score: 42.00',
                    'RODEO score: 51',
                    'macrolactam: ', 'putative cleaved off residues: '
                   ]
        self.assertEqual(expected, core.qualifiers['note'])
        self.assertEqual(orig_feature.qualifiers['locus_tag'],
                         core.qualifiers['locus_tag'])
